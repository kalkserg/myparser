
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;


public class URLConnectionDemo {
    private static ArrayList<String> listImgPath = new ArrayList<>();

    public static void main(String[] args) {
        getImg("http://www.google.com");
    }

    public static void getImg(String web){
        StringBuilder sb = new StringBuilder();
        URL url = null;

        //Get HTML
        try {
            url = new URL(web);
            URLConnection urlcon = url.openConnection();
            InputStream stream = urlcon.getInputStream();

            int ch;
            while ((ch = stream.read()) != -1){
                sb.append((char) ch);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        //Get All Image Path
        listImgPath = getImgPath(sb);

        //Create Output Dir
        createDir(url);

        try{
            for (String filename: listImgPath) {
                ReadableByteChannel readChannel = Channels.newChannel(new URL(url + filename).openStream());
                FileOutputStream fileOS = new FileOutputStream(url.getHost()+"/"+Path.of(url.getHost() + filename).getFileName());
                FileChannel writeChannel = fileOS.getChannel();
                writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);
            }
        }
        catch (IOException e){
            System.err.println("Failed to save file!" + e.getMessage());
        }
    }

    public static void createDir(URL url) {
        Path path = Path.of(url.getHost());
        System.err.println(path);
        try {
            Files.createDirectories(path);
        }
        catch (IOException e){
            System.err.println("Failed to create directory!" + e.getMessage());
        }
    }

    public static ArrayList<String> getImgPath(StringBuilder str){
        ArrayList<String> imgPaths = new ArrayList<>();
        String str0 = str.toString();
        String str1,str2,str3;

        while(str0.indexOf("<img")!=-1) {
            str1 = str0.substring(str0.indexOf("<img"));
            str2 = str1.substring(str1.indexOf("src="));
            str3 = str2.substring(4, str2.indexOf(" "));
            imgPaths.add(str3.substring(1, str3.length() - 1));
            str0 = str2;
        }
        return imgPaths;
    }

}
